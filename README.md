Url de l'application : [https://hildynov.gitlab.io/triade/](https://hildynov.gitlab.io/triade/) 

Documentation API Rest : https://gitlab.com/hildynov/triade/-/blob/master/documentation/api-triade.md

Journal de bord : https://gitlab.com/hildynov/triade/-/blob/master/documentation/journal_de_bord.md

Phase de conception : https://gitlab.com/hildynov/triade/-/blob/master/documentation/Phase_conception.md

Sources code serveur backend : https://gitlab.com/hildynov/triade/-/tree/master/serveur