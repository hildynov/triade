Étapes parcoures pour la création du projet "Triade"
-
 - État de l'art & veille des techno/applications existantes
 - Création du concept de l’application
 - Maquettes de notre application
 - Utilisation de Mobirise pour récupérer la structure minimale de notre application pwa
 - Intégration du manifest.json
 - Intégration du service-workeur.js
 - Mise en forme des pages front : 
    - Ergonomie
    - Responsive
    - Logo
    - Images / couleurs
    - Typographie 
    
- Utilisation du javascript pour les fonctionnalités natives du téléphone :
    - Audio
    - Vibrations
    - Notifications
    - Cache storage

- Mise en place de de l’architecture back (diagramme de classes / UML )
- Mise en place de la base de données
- Mise en place du framework PHP CodeIgniter pour la partie backend
- Développement des fonctionnalités d’inscription / connexion côté backend
- Utilisation du JQuery pour la communication front et back
- Phase de tests / Corrections (méthode agile)
- Livraison de la V1.0 de l'application Triade