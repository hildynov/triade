**Connexion**
----
  Retourne les infos de l'utilisateur.

* **URL**

https://api-triade.romainhild.com/index.php/api/login

* **Method:**

  `POST`
  
* **Data Params**
  ```gherkin
| Name     | Value|
| ---      | ---       |
| email| romainhild@gmail.com         |
| password     | toto        |

* **Success Response:**

  * **Code:** 200 OK
    **Content:** 
      ```
    {
		"status": true,
		"message": "User login successful.",
		"data": {
			"id": "1",
			"email": "romainhild@gmail.com",
			"password": "f71dbe52628a3f83a77ab494817525c6"
			}
	} 
	```

* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:** 
	```
	{
		"status": false,
		"message": "Wrong email or password."
	} 
	```
 



**Inscription**
----
  Inscrit l'utilisateur en base.

* **URL**

https://api-triade.romainhild.com/index.php/api/registration

* **Method:**

  `POST`
  
* **Data Params**
  ```gherkin
| Name     | Value|
| ---      | ---       |
| email| newmail@gmail.com         |
| password     | newpassword        |


* **Success Response:**

  * **Code:** 200 OK
    **Content:** 
      ```
    {
		"status": true,
		"message": "The user has been added successfully.",
		"data": "7"
	} 
	```

* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:** 
	```
	{
		"status": false,
		"message": "The given email already exists."
	} 
	```
