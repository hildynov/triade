Réalisé par Romain Hild, Romain Hamel & Romain De Jesus Dias


# Etat de l'art

### Twitter Lite 
Une mise à jour majeure du réseau social pour la version mobile de son site web.
Le réseau social aux 280 caractères a opéré un important changement en avril 2017 en décidant d’abandonner la version mobile classique de son site au profit d’une Progressive Web App. Le développement de Twitter Lite en PWA a pour but de répondre à plusieurs objectifs :
•	Amélioration de la fluidité grâce à un chargement de l’application quasi instantané.
•	Augmentation de l’engagement des utilisateurs sur mobile
•	Réduction de la consommation de données.
Twitter souhaitait pouvoir offrir à tous ses utilisateurs, une expérience mobile solide, rapide et fiable, quelque soit leur zone géographique ou la qualité de leur accès à internet.
Un allègement des fonctionnalités
Avec Twitter Lite, le réseau social s’est recentré sur les fonctionnalités de base de son application, à savoir l’envoi de tweets et les retweets, les messages privés, ainsi que l’upload de photos. A contrario, les fonctions suivantes ne sont pour le moment pas encore disponibles sur la web app : la publication de vidéos en direct via Periscope, l’édition de photos et la création de “moments”.
Ce retour aux fonctionnalités de base permet à Twitter Lite d’afficher des performances admirables aussi bien en termes de rapidité d'exécution et de chargement que pour sa consommation de data.
Une stratégie qui lui permet de viser les pays en voie de développement et les zones de faible connectivité
Cette version mobile allégée et rapide de Twitter a été pensée initialement pour les pays émergents où la connexion réseau ne s’étend pas encore sur tout le territoire. Grâce à l’intégration d’un Service Worker et l’utilisation judicieuse du cache, l’application est moins gourmande en terme de stockage mais aussi en terme de consommation des données.
En combinant les technologies du web aux fonctionnalités natives des applications mobiles, Twitter Lite rend possible l’indexation de sa Progressive Web App sur les moteurs de recherche mais aussi l’envoi de notifications push web ou l’intégration d’une bannière proposant aux utilisateurs d’ajouter en un clic la PWA sur l’écran d’accueil du mobile.
Les avantages de Twitter Lite face à l’application mobile
Les bénéfices pour Twitter liés au développement de la Progressive Web App sont nombreux :
•	Le premier concerne la quantité d’espace de stockage nécessaire à l’utilisation de l’application très économe puisqu’elle représente moins de 1Mo.
•	Il en est de même pour la consommation de données, Twitter Lite permet une réduction de 70% par rapport aux applications mobiles natives.
•	L’utilisation du cache grâce à la mise en place d’un Service Worker a permis une nette réduction du temps de chargement.
•	Egalement parmi les avantages, on note l’augmentation de l’engagement sur mobile grâce à l’envoi de notifications push via le navigateur et la possibilité d’installer la Progressive Web App directement sur l’écran d’accueil du mobile.
Bilan : quelques chiffres
Nicolas Gallagher, responsable de l’ingénierie chez Twitter fait un bilan chiffré des apports de Twitter Lite par rapport à l’ancienne version mobile :
•	Twitter Lite a permis d’augmenter de 65% le nombre de pages par session
•	Le nombre de Tweets envoyés s’est vu amélioré de 75%
•	Le taux de rebond a, quant à lui, diminué de 20%.
Grâce à la mise en place de la Progressive Web App, Twitter Lite est devenu la méthode la plus rapide et la plus sûre d'utiliser Twitter sur mobile. Les performances et fonctionnalités principales sont similaires à celles fournies par l’application native mais Twitter Lite requiert seulement moins de 3% de l'espace de stockage en comparaison à Twitter pour Android.

### Airbnb
Airbnb permet à des particuliers de louer tout ou une partie de leur propre habitation9 comme logement d'appoint. Le site offre une plateforme de recherche et de réservations entre la personne qui offre son logement et le vacancier qui souhaite le louer. Il couvre plus de 1,5 million d'annonces dans plus de 34 000 villes et 191 pays. Pour les expériences, les habitants partagent leur vie sociale, leurs intérêts et travail à un prix. Et pour ce qui concerne les lieux, des guides experts, connaissant bien les lieux, proposent des adresses de lieux conseillés selon des critères spécifiques

Le portail met en relation des internautes offrant ou recherchant des chambres, appartements, maisons et châteaux, voire des cabanes et autres yourtes. Dans la moitié des cas, l'hébergeur vit sur place. Olivier Masselis, fondateur d’Evaway.com, est installé à San Francisco depuis six mois. Les loyers étant prohibitifs, ce Français à l’esprit start-up sous-loue le clic-clac de son salon privatisable 70$ la nuit, un bon tarif pour son toit situé en centre-ville

Cela fait cinq ans, jour pour jour, qu'Airbnb propose aux particuliers de sous-louer leurs logements. La jeune pousse californienne propose déjà plus de 300 000 logements dans 34 000 villes et 192 pays… Chaque jour, 140 000 personnes sont logées chez des particuliers grâce à elle. L'an passé, 3 millions de nuits ont été réservées par des voyageurs à la recherche de vacances pas chères et différentes.
Près d'un million de Français ont même séjourné dans leur propre région de résidence. Les plus casaniers sont les habitants d'Auvergne-Rhône-Alpes, de Nouvelle Aquitaine, d'Occitanie et de Provence-Alpes-Côte d'Azur. Plus étonnant, plus de 90 000 Franciliens ont trouvé refuge en région parisienne.
A la recherche de bons plans des vacanciers répond, du côté des propriétaires - « hôtes » dans le langage Airbnb-, la volonté de gonfler leurs revenus. Avec des prix variant de 48 euros la nuit en moyenne en Bourgogne ou dans le Grand-Est à 74 euros sur la Côte d'Azur, 89 euros en Ile-de-France et 150 euros à Paris, un hôte qui louerait un mois en été peut gagner entre 1500 et 4500 euros. Sur l'année, « le revenu médian est de 2000 euros », nuance Emmanuel Marill

### Sites tendances

https://matruecannabis.com/fr
matruecannabis a été fondée en 2018 avec l’objectif de redéfinir en profondeur le marché du Cannabis mondial. Basés en Suisse, nous produisons avec fierté les meilleurs Fleurs de Cannabis au monde, dans le respect absolu des limites imposées par la loi. 
La production de Ma true cannabis est réalisée indoor, de manière durable, novatrice et biologique. MA true cannabis est une marque qui respecte et encourage activement la Responsabilité Sociale de l’Entreprise : valorisation de nos collaborateurs, ambiance de qualité au travail, motivation de nos employés, contrôles de nos filières pour certifier l’absence de travail des mineurs ne sont que quelques exemples de notre esprit d’entreprise.

Système de scroll 
Immersion et interactivité avec le public 
Ergonomie incroyable


https://www.deejo.fr/fr/
Ce site est destiné à la coutellerie.
Les photos vont de pairs en suivant le scroll.
 Utilisation de cards pour présenter les différentes marchandises en vente.

https://koox.co.uk

Ce site est destiné à l’art culinaire. La cible principale sont les londoniens.
Les réseaux sociaux sont présents tout en haut à droite de la page.
Le design est très épuré, peu de contenu ce qui favorise la prise d’informations.
Ce site a un design redoutable avec des croquis originaux
Le système de scroll est très bien fait.
Les menus interactifs sont positionnés sur les côtés ainsi que le logo. 
La police et la typographie utilisé est grasse.

https://square.geex-arts.com/

Ce site sous forme d’énigme à une interactivité exceptionnelle avec un petit carré qui sert de souris
Tout le site est construit sur un jeu interactif.
Interactions et animations verticales/horizontales

### Application Mobile
Revolut
Cette application bancaire est basée sur le principe d’instantanéité. Il est possible de lier une ou plusieurs banques au compte revolut. Lr principe est que tous les virement inter-Revolut sont instantanés et gratuits. 

 

Waze
Comment l'application Waze fonctionne-t-elle ?

Mécanisme de navigation Waze

L'efficacité de Waze repose sur vous. Lorsque vous conduisez avec l'application Waze ouverte sur votre appareil, vous partagez des informations en temps réel sur les conditions de circulation et la structure de la route. Lorsque vous utilisez Waze, vous pouvez également renseigner activement la communauté sur la densité du trafic, les accidents, les contrôles de police, les routes bloquées, les conditions météorologiques et bien plus encore. Waze recueille ces informations et les analyse instantanément afin de fournir aux autres wazers l'itinéraire optimal vers leur destination, 24 heures sur 24.
Pour améliorer le service, le moyen le plus facile consiste tout simplement à garder l'application Waze ouverte lorsque vous conduisez. Même si vous n'avez pas besoin de Waze pour vous guider, ouvrez l'application. Vous n'avez rien d'autre à faire.
À l'aide des informations qu'elle recueille, l'application Waze calcule la vitesse moyenne, recherche les éventuelles erreurs, rectifie le tracé des routes, et mémorise le sens de circulation et les virages. Vous n'avez pas besoin de suivre un itinéraire particulier avec Waze. En fait, l'application est plus efficace lorsque vous l'utilisez pour vos déplacements habituels et pour vos trajets domicile-travail.
Les données des cartes et de la navigation sont renseignées par les utilisateurs. Plus les conducteurs ouvrent Waze, plus la navigation est précise.
À qui l'application Waze est-elle destinée ?

L'application Waze est alimentée par des utilisateurs du monde entier. Les conducteurs sont connectés les uns aux autres et contribuent tous ensemble à améliorer l'expérience de conduite de chacun. En tant qu'application de trafic et de navigation basée sur la communauté, Waze est conçue à l'origine comme un média social de navigation destiné aux voitures privées. Pour cette raison, notre outil de navigation n'inclut pas actuellement les voies réservées aux transports en commun, aux vélos ni aux poids lourds.
Comment l'application Waze apprend-elle vos itinéraires ?
Pour que l'application Waze vous fournisse précisément l'itinéraire optimal, selon vos paramètres, elle doit disposer d'informations exactes sur l'ensemble des segments et itinéraires à proximité. Waze recueille des données sur chaque route parcourue par un conducteur avec l'application ouverte.  Ainsi, lorsque vous conduisez sur une route avec l'application ouverte, Waze dispose de plus de données pour comparer chaque itinéraire possible et vous suggérer la meilleure option.
Nous améliorons continuellement l'algorithme du système afin de vous proposer l'itinéraire optimal. Cependant, il peut parfois exister un itinéraire meilleur que celui suggéré par Waze. Ces erreurs se produisent, car le système repose sur des statistiques moyennes en temps réel. Nous vous conseillons de suivre l'itinéraire suggéré plusieurs fois pour que Waze puisse dégager des informations.
Si vous pensez que Waze ne vous fournit pas le meilleur itinéraire, la raison peut être l'une des suivantes :
•	Une erreur de carte quelque part sur votre itinéraire favori
•	Un manque de données correctes concernant la vitesse/circulation sur cet itinéraire
•	Un manque de données correctes concernant la vitesse/circulation sur l'itinéraire suggéré
Connexion Internet

L'application Waze a été conçue en partant du principe que le conducteur dispose d'une connexion au réseau de données en permanence (ou au moins la plupart du temps). Pour que Waze fonctionne de façon optimale, votre connexion de données doit être active sur votre appareil mobile. Cette connexion permet d'enrichir Waze d'informations sur le trafic en temps réel et de maintenir la carte à jour. Sans connexion Internet, vous ne pourrez pas obtenir d'itinéraire ni lancer la navigation.
Si vous disposez d'une connexion par intermittence, Waze s'efforcera de récupérer sur les serveurs les données concernant les alertes sur le trafic et les dangers, mais les informations ne seront peut-être pas fiables. En outre, si l'application ne peut pas se connecter aux serveurs Waze, vous ne pourrez pas publier les dangers. Waze n'enregistre pas les signalements ni les erreurs de carte dans la mémoire cache afin de les envoyer plus tard.


# Phase de conception
#### Personas : 
1. Julien
Julien est un petit enfant de 3 ans. Il a déjà appris à marcher mais ne sait pas encore compter jusque 10 ni même réciter l’alphabet en entier. Généralement Julien aime les petits jeux ludiques et ne s’en lasse jamais.

2. Gabriel
Gabriel est père de deux filles âgées de 3 et 4 ans. Il souhaite les initier à la prononciation de lettres afin de bien les préparer pour la rentrée scolaire. Toutefois les manuels scolaires ne sont pas suffisants car ils n’émettent aucun son. Notre père de famille s’intéresse donc aux applications scolaires existantes pour enfants.

#### Scenario : 
Comment apprendre l'alphabet ?
Julien ne sait pas encore compter jusque 10 mais ne se laisse pas abattre. À son âge il est complétement dépendant de ses parents qui lui imposent une éducation assez soutenue. Julien apprend d’abord à compter avant de passer à l’alphabet. Pour cela son père lui met à disposition un Ipad où l’application « EDUQ » est déjà préinstallée. 


Développer ses facultés d'audition et de mémorisation
Le petit Julien doit non seulement apprendre à prononcer des lettres mais aussi des chiffres, mais aussi à les mémoriser. Pour cela il s’entraine avec des manuels scolaires mais il s’impatiente vite car il n’y a pas d’interactivité entre lui et le manuel. Son père à trouvé une solution efficace pour le motiver, l’utilisation d’un outil digital. En effet, le jeune papa a installé l’application « EDUQ » qui prononcera les mots dans la langue choisie ce qui permettra à l’enfant de mieux retenir les mots du jeu

### Tâches utilisateurs :

1. Apprendre l'alphabet. Pour cela, l'utilisateur pourra selectionner la catégorie alphabet et cliquer sur chacune des lettres. L'assistance vocale du telephone pourra alors parler et dire la lettre à voix haute.

2. Apprendre les nombres. De la même manière, l'enfant pourra apprendre les nombres en appuyant simplement dessus. Lors du clic, le son du telephone se declenchera et laissera entendre à l'enfant le son correspondant au nombre selectionné.

3. Deux autres tâches sont possibles, en effet l'enfant peux choisir d'apprendre les jours de la semaine ou encore les couleurs.

4.  Autres fonctionnalités : Une notification apparaitra à l'ecran lors du lancement et le vibreur se mettra en marche.

### Arbre de tâches :

![enter image description here](https://imagizer.imageshack.com/img924/1341/z1qBLz.png)

### Maquettes: 
![Premiere page](https://imagizer.imageshack.com/img922/9074/Lkanmg.png)
![Seconde page : Le menu](https://www.zupimages.net/up/20/14/x5fd.png)
![Le jeu de l'alphabet] (https://www.zupimages.net/up/20/14/6atl.png)
![Le jeu des chiffres](https://www.zupimages.net/up/20/14/961v.png)
![Le jeu des couleurs](https://www.zupimages.net/up/20/14/48wv.png)
![Le jeu des jours](https://www.zupimages.net/up/20/14/961v.png)
![Dernière page](https://www.zupimages.net/up/20/14/u9g7.png)




--- 
