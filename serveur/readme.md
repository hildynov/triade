Notre partie backend est hébergée sur un serveur privé (IONOS), c'est pourquoi il n'est pas rendu public et par conséquent, n'est pas accessible. Nous vous donnons tout de même la possibilité de visionner les sources dédiées au backend de notre application depuis cette répertoire.

Le framework php que nous utilisons (CodeIgniter) respecte une architecture MVC (Model, Vue, Controller), pour la partie backend de l'application nous n'aurons pas besoin de la couche "Vue", c'est pourquoi nous vous présentons uniquement le Model et le Controller.

Le fichier model.php permettra de récupérer toutes les informations relatives à nos utilisateurs depuis notre base de données en utilisant les active records (syntaxe propre au framework CodeIgniter). Il sera appelé par le fichier controller.php qui sera chargé de formater toutes les données récoltées afin de les retourner à notre serveur applicatif.
