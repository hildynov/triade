<?php 

/* Recherche d'un utilisateur en base */
function getRows($params = array())
{
    $this->db->select('*');
    $this->db->from('utilisateurs');

    // effectue un where sur champs en parametre
    if(array_key_exists("conditions",$params)){
        foreach($params['conditions'] as $key => $value){
            $this->db->where($key,$value);
        }
    }
    
    // si on trouve un utilisateur
    if(array_key_exists("id",$params)){
        $this->db->where('id',$params['id']);
        $query = $this->db->get();
        $result = $query->row_array();
    }
    // retounrne une chaine vide si l'utilisateur n'existe pas en base
    else{
        $result = "";    
    }
    //retourne l'utilisateur
    return $result;
}

/* insertion d'un utilisateur en base */
public function insert($data)
    {
    
        //insertion des l'utilisateur en base
        $insert = $this->db->insert('utilisateurs', $data);
            
        //retourne les informations de l'utilisateur
        return $insert?$this->db->insert_id():false;
    }

?>