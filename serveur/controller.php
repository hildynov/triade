	<?php

    /* inscription */
    public function registration_post() {
        // connexion à la database
        $this->load->database();

        // récupère le mail & mot de passe
        $email = ($this->post('email'));
        $password = $this->post('password');

        // vérifie si les champs sont renseignés
        if(!empty($email) && !empty($password)){
            
            // Vérification si l'email donné existe
            $con['conditions'] = array(
                'email' => $email,
            );
            $userCount = $this->Api_model->getRows($con);

            if($userCount > 0){
                // initialise la réponse et exit
                $this->response([
	                'status' => FALSE,
	                'message' => 'Cette adresse email existe déjà.'
	            ], REST_Controller::HTTP_OK);
            }else{
                
                $userData = array(
                    'email' => $email,
                    'password' => md5($password)
                );
                // ajoute les données en base
                $insert = $this->Api_model->insert($userData);
                
                // vérifie que l'utilisateur est bien enregistré
                if($insert){
                   // initialise la réponse et exit
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Utilisateur ajouté en base.',
                        'data' => $insert
                    ], REST_Controller::HTTP_OK);
                }else{
                    // initialise la réponse et exit
                    $this->response("problèmes rencontrés lors de l ajout.", REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }else{
            // initialise la réponse et exit
            $this->response("Veuillez remplir tous les champs.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	/* connexion */
	public function login_post() {
        // récupération des données
        $email = $this->post('email');
        $password = $this->post('password');

        // vérifie si les champs sont renseignés
        if(!empty($email) && !empty($password)){
            
            // vérifie si le compte existe
            $con['conditions'] = array(
                'email' => $email,
                'password' => md5($password)
            );
            // récupère les information de l'utilisateur
            $user = $this->Api_model->getRows($con);
            
            if($user){
                // initialise la réponse et exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'Connexion réussie.',
                    'data' => $user
                ], REST_Controller::HTTP_OK);
            }else{
                // initialise la réponse et exit
                $this->response([
	                'status' => FALSE,
	                'message' => 'Mauvais identifiants.'
	            ], REST_Controller::HTTP_OK);
                
            }
        }else{
            // initialise la réponse et exit
            $this->response("Veuillez remplir tous les champs.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    ?>